# Тестовые задания
## *Решить необходимо одно из предложенных заданий*

## Общая часть

Результат можно прислать в архиве, залить на любой репозиторий и дать ссылку или прислать ссылку на облачное хранение, любой вариант подойдет.

Список общих команд приложения:

1. /help или -h или —help (любой вариант подойдет) выводит список доступных аргументов
2. Аргумент входного файла или директории
3. Аргумент вывода в файл или в терминал

Плюсами будут:

1. Скрипт сборки и запуска
2. Автотест
3. Оформленный репозиторий

Приложение должно ограничивать запуск если не выбран хотя бы один аргумент поиска и хотя бы один вариант вывода.
Если выбрана директория то поиск внутри всех вложенных файлов.

Версия шарпов может использоваться любая.
## Тестовое задание 1
Необходимо написать консольную утилиту по поиску в XML файлах.

В дочерней директории configs/xml находится несколько XML файлов.
Программа должна уметь выполнять следующие команды:

1. Аргумент поиска по атрибуту
2. Аргумент поиска по значению
3. Вывод выбранного атрибута у найденного элемента
4. Вывод выбранного значения у найденного элемента
5. В случае отсутвия вхождений написать об этом

## Тестовое задание 2
Необходимо написать тестовую утилиту по поиску в Json файлах.

В дочерней директории configs/json находится несколько Json файлов.
Программа должна уметь выполнять следующие команды:

1. Поиск объекта по свойству
2. Вывод наденного объекта
3. Вывод свойства найденного объекта
4. В случае отсутвия вхождений написать об этом

## ЗЫ
На решения данных задач не вводится никаких огранечений на технологии и подходы, любые библиотеки и т.д., единственная важная вещь это корректное решение поставленной задачи.
